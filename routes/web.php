<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PertanyaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.index');
});
Route::get('/data-tables', function () {
    return view('admin.datatable');
});

Route::get('/dashboard', [DashboardController::class, 'index']);
Route::get('/pertanyaan', [PertanyaanController::class, 'index'])->name('datapertanyaan');
Route::get('/pertanyaan/create', [PertanyaanController::class, 'create'])->name('createpertanyaan');
Route::post('/pertanyaan/create', [PertanyaanController::class, 'store'])->name('storepertanyan');
Route::get('/pertanyaan/{id}', [PertanyaanController::class, 'show'])->name('showpertanyan');
Route::get('/pertanyaan/{id}/edit', [PertanyaanController::class, 'edit'])->name('editpertanyan');
Route::put('/pertanyaan/{id}', [PertanyaanController::class, 'update'])->name('updatepertanyan');
Route::delete('/pertanyaan/{id}}', [PertanyaanController::class, 'destoyd'])->name('deletepertanyan');