<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    /* 
    Memanggil view table
    */

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('admin.pertanyaan.datapertanyaan', ['pertanyaan' => $pertanyaan]);
    }

    public function create() {
        return view('admin.pertanyaan.form');
    }

    public function store(Request $request) {
        $query = DB::table('pertanyaan')->insert([
            'judul'=> $request('judul'),
            'isi' => $request('judul')
        ]);
        return redirect('/datapertanyaan');
    }

    public function show($id) {
        $post = DB::table('pertanyaan')->get();
        return view('admin.pertanyaan.view');
    }

    public function edit($id) {
        $post = DB::table('pertanyaan')->where('id', $id)->fist();
        return view('admin.pertanyaan.form');
    }

    public function update($id, Request $request)
    {
        # code...
        $query = DB::table('pertanyaan')->where('id', $id) ->updatee() ([
            'isi' => $request['isi'],
            'judul' => $request['judul'],
        ]);
        return redirect('/datapertanyaan');
    }
    
    public function destroy($id) 
    {
        # code...
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/datapertanyaan');
    }
}
