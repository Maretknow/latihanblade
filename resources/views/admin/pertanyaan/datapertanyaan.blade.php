@extends('layouts.app')
@push('css')
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('asset/admin/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('asset/admin/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('asset/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('asset/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Pertanyaan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Pertanyaan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-12">
        <div class="card">
      <div class="row mb-2">
          <div class="col-sm-6">
          <div class="card-header">
        <h3 class="card-title">Table Data Pertanyaan</h3>
      </div>
          </div><!-- /.col -->
          <div class="col-sm-6">
          <a href="{{ route('createpertanyaan') }}"><button type="button" class="btn btn-block btn-primary btn-flat">Tambah Pertanyaan</button></a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Nomer</th>
            <th>Judul</th>
            <th>Pertanyaan</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          $no = 1;
          foreach ($pertanyaan as $item) {  ?>
            <tr>
            <td><?php echo $no++ ?></td>
            <td> <?php echo $item -> judul ?>
            </td>
            <td> <?php echo $item -> isi ?> </td>
            <td> <a href="{{ route('editpertanyaan') }}"><button type="button" class="btn btn-block btn-warning btn-sm">Edit</button></a>
    <form method="POST" action="{{ route('deletepertanyan') }}">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <div class="form-group">
            <input type="submit" class="btn btn-block btn-danger btn-sm" value="Delete">
        </div>
    </form>
            </td>
          </tr>
          <?php }  ?>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
@endsection

@push('scripts')
<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('asset/admin/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('asset/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE -->
<script src="{{ asset('asset/admin/dist/js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ asset('asset/admin/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('asset/admin/dist/js/demo.js') }}"></script>
<script src="{{ asset('asset/admin/dist/js/pages/dashboard3.js') }}"></script>
<script src="{{ asset('asset/admin/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('asset/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable( {
        "responsive" : true,
        "autoWidth" : false,
    });
  });
</script>
@endpush