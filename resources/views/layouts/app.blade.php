<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Dashboard 3</title>
  @stack('css')
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
@section('nav')
    @include('layouts.nav')
@show
@section('sidebar')
    @include('layouts.sidebar')
@show
@yield('content')
@section('footer')
    @include('layouts.footer')
    @stack('scripts')
@show
</body>
</html>
